# Changelog

## Version 1.0.0b1 - 2017-12-04
### Added
    * Linter support in `setup.py`
    * Change log file
    * Support for RandomState instances

### Changed
    * Switched from an n-iterations-n-tasks model to a n-processes-n-tasks one for multiprocess execution
    * The classes won't use np.random.seed() anymore
