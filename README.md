RELIEF Family algorithm implementations
=====

This module provides support for three **feature selection** algorithms:
 * Relief
 * ReliefF
 * RReliefF

The implementation is *scikit-learn compatible* and supports multiprocessing to
speed up computation. This module is intended for usage with Python 3 only.


Usage
-----
    import sklearn_relief as relief

    # Load some data and put it in a numpy.array matrix
    my_input_matrix = ...

    # Load the label vector
    my_label_vector = ...

    r = relief.Relief(
        n_features=3 # Choose the best 3 features
    ) # Will run by default on all processors concurrently

    my_transformed_matrix = r.fit_transform(
        my_input_matrix,
        my_label_vector
    )

    # my_transformed_matrix will now contain the 3 highest-ranked feature
    # vectors from my_input_matrix.

    # If you are interested in the computed weights, print them with
    print(r.w_) # Each i-th weight will be the weight of the i-th feature


Detailed help
-----

For detailed help, please refer to the in-source documentation:

    $ python
    >>> import sklearn_relief
    >>> help(sklearn_relief)

Or by using `pydoc`:

    $ pydoc sklearn_relief


License
-----

This module is distributed under the BSD 3 clause license. Refer to the
in-source documentation for further information.


Bugs & contributions
-----

Feel free to submit bug reports, patches and contributions to
[the project's GitLab page](https://gitlab.com/qafir/sklearn-relief/ "GitLab").
